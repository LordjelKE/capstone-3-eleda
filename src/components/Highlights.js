import React from 'react';

import {Row,Col,Card, Container} from 'react-bootstrap';

export default function Highlights(object){

	return (
		<Container>
		<Row>
			<Col xs={12} md={4} className="columnCard">
				<Card className="cardHighlight text-white" bg="dark">
				<Card.Header>
					<h4 className="text-center text-success">CAPSTONE</h4>
				</Card.Header>
					<Card.Body>
						<Card.Title>
							<h2 className="text-center">React</h2>
						</Card.Title>
						<Card.Text>
							<h5 className="text-justify text-white">For capstone 3, the web developer used React which is a free and open-source front-end JavaScript library for building user interfaces or UI components. The backend server used was from capstone 2. This is the last capstone for the Zuitt bootcamp.</h5>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4} className="columnCard">
				<Card className="cardHighlight text-white" bg="dark">
				<Card.Header>
					<h4 className="text-center text-success">SHOP</h4>
				</Card.Header>
					<Card.Body>
						<Card.Title>
							<h2 className="text-center">What is The Vault?</h2>
						</Card.Title>
						<Card.Text>
							<h5 className="text-justify text-white">The Vault is an E-commerce website that sells PC components at their retail prices. Due to the recent high demand on computer parts this year, only GPUs are available for purchase. Availability for pre-orders will be informed once the items are restocked.</h5>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4} className="columnCard">
				<Card className="cardHighlight text-white" bg="dark">
				<Card.Header>
					<h4 className="text-center text-success">ABOUT</h4>
				</Card.Header>
					<Card.Body>
						<Card.Title>
							<h2 className="text-center">Web Developer</h2>
						</Card.Title>
						<Card.Text>
							<h5 className="text-justify text-white">He is a beginner full-stack web developer who is currently enrolled and studying at the Zuitt coding bootcamp. His goals are to pursue his passion for coding and work in the I.T. industry.</h5>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		</Container>
		)
}