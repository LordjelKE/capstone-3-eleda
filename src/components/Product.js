import React, {useState, useEffect, useContext} from 'react'

import {Card, Button, Col} from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus, faTimesCircle, faMinusCircle } from '@fortawesome/free-solid-svg-icons'

import UserContext from '../userContext'

export default function Product({productProp}){

	const {user} = useContext(UserContext) 

	const [stocks, setStocks] = useState(10)
	const [isActive, setIsActive] = useState(true)

	useEffect(()=>{
		if (stocks === 0){
			setIsActive(false)
		}
	},[stocks])

	function order(){
		setStocks(stocks - 1);
	}

	return (
		<Col xs={12} md={4} className="columnCard">
			<Card className="cardHighlight text-white" bg="dark">
			<Card.Header>
				<h2 className="text-center text-success">{productProp.productName}</h2>
			</Card.Header>
				<Card.Body>
					<Card.Text className="text-white">
						<h5>{productProp.description}</h5>
					</Card.Text>
					<Card.Text className="text-primary">
						<h3>₱{productProp.price}.00</h3>
					</Card.Text>
					<Card.Text className="text-white">
						<h4>In Stock: {
							stocks === 0 
							?<span className="text-danger">"Out of Stock"</span> 
							: <span className="text-success">{stocks} pcs</span>
						}
						</h4>
					</Card.Text>
				</Card.Body>
				<Card.Footer className="footer">
					 {

					 user.email
					 ?
						isActive === false
						?
						<div className="text-center d-grid gap-2">
						<Button variant ="red" disabled><FontAwesomeIcon icon={faTimesCircle} className="mr-2" />Add to Cart</Button>
						</div>
						:
						<div className="text-center">
						<Button variant ="green" onClick={order}><FontAwesomeIcon icon={faCartPlus} className="mr-2" />Add to Cart</Button>
						</div>
					:
						<div className="text-center d-grid gap-2">
						<Button variant ="red" disabled><FontAwesomeIcon icon={faMinusCircle} className="mr-2" />Add to Cart</Button>
						</div>
					}
 				</Card.Footer>
			</Card>
		</Col>
		)
}