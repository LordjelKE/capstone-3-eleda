import React from 'react';

import {Jumbotron, Row, Col} from 'react-bootstrap'

import {Link} from 'react-router-dom'

export default function Banner({bannerProp}){
	
	return (
			<Row>
				<Col>
					<Jumbotron className="jumbotron mt-5">
						<h1 className="text-center">{bannerProp.title}</h1>
						<p>{bannerProp.description}</p>
						<div className="text-center">
						<Link to={bannerProp.destination} className="btn btn-dark"><h5>{bannerProp.label}</h5></Link>
						</div>
					</Jumbotron>
				</Col>
			</Row>
		)
}