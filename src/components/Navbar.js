import React, {useContext} from 'react';

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBomb } from '@fortawesome/free-solid-svg-icons'

import {NavLink, Link} from 'react-router-dom'

import UserContext from '../userContext'

export default function NavBar(){

	const {user, unsetUser, setUser} = useContext(UserContext) 

	function logout(){
		unsetUser()
		setUser({
			email:null,
			isAdmin:null
		})
		window.location.replace('/login')
	}
                                                                
	return(
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" className="text-center" sticky="top" id="Navbar">
			<Navbar.Brand as={Link} to="/" className="brand"><FontAwesomeIcon icon={faBomb} /> VAULT</Navbar.Brand>
			<Navbar.Toggle aria-controls="responsive-navbar-nav"/>
			<Navbar.Collapse id="responsive-navbar-nav">
				<Nav className="mr-auto">		
					{
						user.isAdmin
						?
						<>
						<Nav.Link as={NavLink} to="/products">Dashboard</Nav.Link>
						<Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
						</>
						:
						<>
						<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
						</>
					}
				</Nav>
				{
					user.email
					?
						user.isAdmin 
						?
						<>
						<Nav>
						<Nav.Link onClick={logout}>Logout</Nav.Link>
						</Nav>
						</>
						:
						<>
						<Nav> 
						<Nav.Link onClick={logout}>Logout</Nav.Link>	
						</Nav>
						</>			
					:
						<>
						<Nav>
							<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						</Nav>
						</>
				}
			</Navbar.Collapse>
		</Navbar>
		)
}