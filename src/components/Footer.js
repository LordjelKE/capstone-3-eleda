import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

export default function Footer(){
	return (
	    <MDBFooter color="black" className="font-small darken-3 pt-0" font="Mate SC" fixed="bot">
	      <MDBContainer>
	        <MDBRow>
	          <MDBCol md="12" className="pt-4">
	            <div className="flex-center">
	              <a href="https://mail.google.com/mail/u/0/#inbox"  target="_blank" className="gmail-ic">
	                <i className="fas fa-envelope-open fa-lg white-text mr-md-4 mr-3 fa-2x">
	                </i>
	              </a>
	              <a href="https://www.linkedin.com/in/lordjel-kin-eleda-421557204/" target="_blank" className="li-ic">
	                <i className="fab fa-linkedin-in fa-lg white-text mr-md-4 mr-3 fa-2x">
	                </i>
	              </a>
	              <a href="https://github.com/Lordjel" target="_blank" className="git-hub">
	                <i className="fab fa-github fa-lg white-text mr-md-4 mr-3 fa-2x">
	                </i>
	              </a>
	            </div>
	          </MDBCol>
	        </MDBRow>
	      </MDBContainer>
	      <div className="footer-copyright text-center py-3 pr-4">
	        <MDBContainer fluid>
	          &copy; {new Date().getFullYear()} | {" "}
	          <a> Lordjel Kin Eleda </a>
	        </MDBContainer>
	      </div>
	    </MDBFooter>
	  );
}