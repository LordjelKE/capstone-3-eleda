import React from 'react'

import Banner from '../components/Banner'

export default function NotFound(){
	let bannerContent = 
	    {
	      title: "Page not found or does not exist",
	      description: "",
	      label: "Back to Home Page",
	      destination: "/"
	    }

	return (
			<Banner bannerProp={bannerContent}/>
		)
}