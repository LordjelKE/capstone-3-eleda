import React, {useState, useEffect, useContext} from 'react'

import {Table, Button, Container, Row, Card} from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArchive, faCheck} from '@fortawesome/free-solid-svg-icons'

import Product from '../components/Product'

import UserContext from '../userContext'

export default function Products(){
	const {user} = useContext(UserContext)

	const [allProducts, setAllProducts] = useState([])
	const [activeProducts, setActiveProducts] = useState([])
	const [update, setUpdate] = useState("")

	useEffect(()=> {
		
		fetch('https://frozen-earth-09956.herokuapp.com/api/products/findAll')
		.then(response => response.json())
		.then(data => {

			setAllProducts(data.IsActiveProducts)

			let productsTemp = data.IsActiveProducts

			let tempArray = productsTemp.filter(product => {
				return product.isActive === true
			})

			setActiveProducts(tempArray)
		})
	},[update])

	let productComponents = activeProducts.map(product=> {

	  return (
	  		<Product key={product._id} productProp={product}/>
	  	)
	})

	function archive(productId){
		let token = localStorage.getItem('token')

		fetch(`https://frozen-earth-09956.herokuapp.com/api/product/archive/admin/${productId}`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUpdate({})
		})
	}

	function activate(productId){
		let token = localStorage.getItem('token')

		fetch(`https://frozen-earth-09956.herokuapp.com/api/product/activate/admin/${productId}`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUpdate({})
		})
	}

	let productRows = allProducts.map(product=>{
		return (

				<tr key={product._id}>
					<td className="text-center text-white"><h5>{product._id}</h5></td>
					<td className="text-center text-white"><h5>{product.productName}</h5></td>
					<td className={product.isActive 
						? "text-success"
						: "text-danger"
					}>
					<div className="text-center">
					<h5>
					{
						product.isActive === true
						? "In Stock"
						: "Out of Stock"
					}
					</h5>
					</div>
					</td>
					<td>
					{
						product.isActive === true
						?
						<div className="text-center">
						<Button variant="danger" className="mx-2" onClick={()=>archive(product._id)}><FontAwesomeIcon icon={faArchive} className="mr-2" />Archive</Button>
						</div>
						:
						<div className="text-center">
						<Button variant="success" className="mx-2" onClick={()=>activate(product._id)}><FontAwesomeIcon icon={faCheck} className="mr-2" />Activate</Button>
						</div>
					}
					</td>
				</tr>
			)
	})

	return (

			user.isAdmin === false || user.isAdmin === null
			?
			<>
			<div className="text-center py-5">
				<Card className="title">
					<Card.Title className="pt-3" id="heading">
						<h2>PRODUCTS</h2>
					</Card.Title>
				</Card>
				<Container className="mt-5 mb-5">
					<Row>
						{productComponents}
					</Row>
				</Container>
			</div>
			</>
			:
			<>
			<div className="py-5">
				<Card className="title">
					<Card.Title className="text-center pt-3" id="heading">
						<h2>ADMIN DASHBOARD</h2>
					</Card.Title>
				</Card>

			<Card className="cardHighlight2 mt-5" bg="dark">
				<Card.Body>
				<Table striped bordered hover>
					<thead>
						<tr className="text-white">
							<th className="text-center"><h4>Product ID</h4></th>
							<th className="text-center"><h4>Product Name</h4></th>
							<th className="text-center"><h4>Availability</h4></th>
							<th className="text-center"><h4>Action</h4></th>
						</tr>
					</thead>
					<tbody>
						{productRows}
					</tbody>
				</Table>
				</Card.Body>
			</Card>	
			</div>
			</>
		)
}