import React, {useState, useEffect, useContext} from "react"

import {Form, Button, Card} from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle} from '@fortawesome/free-solid-svg-icons'

import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Redirect} from 'react-router-dom'

export default function AddProduct(){
	const {user} = useContext(UserContext)

	const [productName, setProductName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")

	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(()=>{

		if(productName !== "" && description !== "" && price !== ""){

			setIsActive(true)

		}else {
			setIsActive(false)
		}
	},[productName, description, price])
	
	function addProductAdmin(e){
		e.preventDefault()

		let token = localStorage.getItem('token')
		fetch('https://frozen-earth-09956.herokuapp.com/api/product/add/admin', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Add Product Failed.",
					text: data.message
				}) 
			} else {

				Swal.fire({
					icon: "success",
					title: "Product added Successfully!",
					text: "Product has been added."
				})
				setWillRedirect(true)
			}
		})
		setProductName("")
		setDescription("")
		setPrice("")
	}

	return (

			(user.isAdmin === true) || willRedirect
			?
			<>
			<div className="py-5">
				<Card className="title">
					<Card.Title className="text-center pt-3" id="heading">
						<h2>ADD PRODUCT</h2>
					</Card.Title>
				</Card>

			<Card className="cardHighlight2 mt-5 text-white" bg="dark">
			<Form onSubmit={e=>addProductAdmin(e)}>
			<Card.Body>
			
				<Form.Group>
					<Form.Label>Product Name:</Form.Label>
					<Form.Control type="text" placeholder="Product Name" value={productName} onChange={e=>{
						setProductName(e.target.value)}
				} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" placeholder="Description" value={description} onChange={e=>{
						setDescription(e.target.value)}
				} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control type="text" placeholder="Price" value={price} onChange={e=>{
						setPrice(e.target.value)}
				} required/>
				</Form.Group>
		
			</Card.Body>
				<Card.Footer className="d-grid gap-2">
				<div className="text-center">
				{
					isActive
					? <Button variant="green" type="submit"><FontAwesomeIcon icon={faPlusCircle} className="mr-2" />ADD</Button>
					: <Button variant="red" type="submit" disabled><FontAwesomeIcon icon={faPlusCircle} className="mr-2" />ADD</Button>
				}
				</div>
				</Card.Footer>
			</Form>
			</Card>
			</div>
			</>
			:
			<Redirect to="/login"/>
		)
}