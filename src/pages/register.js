import React, {useState, useEffect, useContext} from 'react'

import {Form, Button, Card} from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPenFancy } from '@fortawesome/free-solid-svg-icons'

import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Redirect} from 'react-router-dom'

export default function Register(){

	const {user} = useContext(UserContext)

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [shippingAddress, setShippingAddress] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")

	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && shippingAddress !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){

			setIsActive(true)

		}else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,shippingAddress,password,confirmPassword])

	function registerUser(e){
		e.preventDefault()

		fetch('https://frozen-earth-09956.herokuapp.com/api/register', {

			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				shippingAddress: shippingAddress,
				password: password
			})
		})
		.then(response => response.json()) 
		.then(data => {

			console.log(data)

			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Registration Failed.",
					text: data.message
				}) 
			} else {
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					text: "Thank you for registering."
				})

				setWillRedirect(true)
			}
		})

		//reset input states/ states bound to input to their initial values

		setFirstName("")
		setLastName("")
		setEmail("")
		setMobileNo("")
		setShippingAddress("")
		setPassword("")
		setConfirmPassword("")
	}

	return (

			user.email || willRedirect
			?
			<Redirect to="/"/>
			:
			<>
			<div className="py-5">
				<Card className="title">
					<Card.Title className="text-center pt-3" id="heading">
						<h2>REGISTRATION</h2>
					</Card.Title>
				</Card>

				<Card className="cardHighlight2 mt-5 text-white" bg="dark">
				<Form onSubmit={e=>registerUser(e)}>
					<Card.Body>
						<Form.Group>
							<Form.Label>First Name:</Form.Label>
							<Form.Control type="text" placeholder="First Name" value={firstName} onChange={e=>{
								setFirstName(e.target.value)}
						} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Last Name:</Form.Label>
							<Form.Control type="text" placeholder="Last Name" value={lastName} onChange={e=>{
								setLastName(e.target.value)}
						} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Email:</Form.Label>
							<Form.Control type="email" placeholder="Email" value={email} onChange={e=>{
								setEmail(e.target.value)}
						} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Mobile Number:</Form.Label>
							<Form.Control type="number" placeholder="11 Digit Mobile No." value={mobileNo} onChange={e=>{
								setMobileNo(e.target.value)}
						} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Shipping Address:</Form.Label>
							<Form.Control type="text" placeholder="Shipping Address" value={shippingAddress} onChange={e=>{
								setShippingAddress(e.target.value)}
						} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Password:</Form.Label>
							<Form.Control type="password" placeholder="Password" value={password} onChange={e=>{
								setPassword(e.target.value)}
						} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Confirm Password:</Form.Label>
							<Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={e=>{
								setConfirmPassword(e.target.value)}
						} required/>
						</Form.Group>
					
					</Card.Body>
					<Card.Footer>
					{
						isActive
						? 
						<div className="text-center">
						<Button variant="green" type="submit"><FontAwesomeIcon icon={faPenFancy} className="mr-2" />Register</Button>
						</div>
						:
						<div className="text-center">
						<Button variant="red" type="submit" disabled><FontAwesomeIcon icon={faPenFancy} className="mr-2" />Register</Button>
						</div> 
					}
					</Card.Footer>
					</Form>
				</Card>
				
				</div>
			</>
		)
}