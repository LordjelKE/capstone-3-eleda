import React, {useContext} from 'react'

import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

import UserContext from '../userContext'

export default function Home(){

	const {user} = useContext(UserContext)

	let bannerContent = 
	    {
	      title: "Welcome to The Vault!",
	      description: "",
	      label: "Register Now!",
	      destination: "/register"
	    }

	let bannerContent2 = 
	    {
	      title: "Welcome to The Vault!",
	      description: "",
	      label: "Order Now!",
	      destination: "/products"
	    }	

	let bannerContent3 = 
	    {
	      title: "Welcome to The Vault!",
	      description: "",
	      label: "Dashboard!",
	      destination: "/products"
	    }

	return (

		user.email
		?
			user.isAdmin
			?
			<>
				<Banner bannerProp={bannerContent3}/>
				<Highlights/>
			</>
			:
			<>
				<Banner bannerProp={bannerContent2}/>
				<Highlights/>
			</>
		:
		<>
			<Banner bannerProp={bannerContent}/>
			<Highlights/>
		</>
		)
}