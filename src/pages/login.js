import React, {useState, useEffect, useContext} from 'react'

import {Form, Button, Card} from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserCheck, faUser } from '@fortawesome/free-solid-svg-icons'

import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Redirect} from 'react-router-dom'

export default function Login(){

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const [isActive, setIsActive] = useState(false)

	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(()=>{

		if((email !== "" && password !== "")){

			setIsActive(true)

		}else {
			setIsActive(false)
		}
	},[email,password])

	function loginUser(e){
		e.preventDefault()

		fetch('https://frozen-earth-09956.herokuapp.com/api/login', {

			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})

		.then(response => response.json()) 
		.then(data => {

			console.log(data)

			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Login Failed.",
					text: data.message
				}) 
			} else {

				localStorage.setItem('token', data.accessToken)
				fetch('https://frozen-earth-09956.herokuapp.com/api/profile', {

					headers:{
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(response => response.json())
				.then(data => {

				localStorage.setItem('email', data.email)
				localStorage.setItem('isAdmin', data.isAdmin)
				setUser({
					email: data.email,
					isAdmin: data.isAdmin
				})

				setWillRedirect(true)

				Swal.fire({
					icon: "success",
					title: "Login Successful!",
					text: `Thank you for logging in, ${data.firstName}`
				})

				})
			}
		})
		setEmail("")
		setPassword("")
	}

	return (

			user.email || willRedirect
			? 
				user.email
				?
				<Redirect to ='/'/>
				:
				<Redirect to ='/products'/>
			: 
			<>
			<div className="py-5">
				<Card className="title">
					<Card.Title className="text-center pt-3" id="heading">
						<h2>LOGIN</h2>
					</Card.Title>
				</Card>

			<Card className="cardHighlight2 mt-5 text-white" bg="dark">
			<Form onSubmit={e=>loginUser(e)}>
				<Card.Body>
					
						<Form.Group controlId="userEmail">
							<Form.Label>Email:</Form.Label>
							<Form.Control type="email" placeholder="Email" value={email} onChange={e=>{
								setEmail(e.target.value)}
						} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Password:</Form.Label>
							<Form.Control type="password" placeholder="Password" value={password} onChange={e=>{
								setPassword(e.target.value)}
						} required/>
						</Form.Group>
					
				</Card.Body>
				<Card.Footer>
				{
					isActive
					? 
					<div className="text-center">
					<Button variant="green" type="submit"><FontAwesomeIcon icon={faUserCheck} className="mr-2" />Login</Button>
					</div>
					: 
					<div className="text-center">
					<Button variant="red" type="submit" disabled><FontAwesomeIcon icon={faUser} className="mr-2"/>Login</Button>
					</div>
				}
				</Card.Footer>
				</Form>
			</Card>
			</div>
			</>
		)
}