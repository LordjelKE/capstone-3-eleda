import React, {useState} from 'react';
import './App.css'
import {Container} from 'react-bootstrap'

/*import react-router-dom components*/
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

/*import components here*/
import NavBar from './components/Navbar'
import Footer from './components/Footer'

/*import pages here*/
import Home from './pages/home'
import Register from './pages/register'
import Login from './pages/login'
import Products from './pages/products'
import AddProduct from './pages/addProduct'
import NotFound from './pages/notFound'

/*import provider*/
import {UserProvider} from './userContext'

/*Function App*/
function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === "true"
  })

  function unsetUser(){
    localStorage.clear()
  }

  return (

    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <NavBar/>
        <div className="switch">
        <Container>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/products" component={Products}/>
            <Route exact path="/addProduct" component={AddProduct}/>   
          <Route component={NotFound}/>
          </Switch>
          </Container>
          </div>
        <Footer/>
      </Router>
    </UserProvider>
    </>
  )
}

export default App;
